const express = require('express')
const cors = require('cors')
const { logger } = require('./Logger')
const { getListaEsercizi, registraWorkoutEsercizi, searchWorkoutByUtente } = require('./queryDb/QueryEsercizi.js')
const { RegistraWorkout } = require('./pojo/RegistraWorkout')

const app = express()

app.use(express.json())
app.use(cors())

//lista esercizi
app.get('/listaesercizi', async (req, res) => {
    let result = []
    logger.info('inizio operazioni lista esercizi')
    result = await getListaEsercizi()
    logger.info('Fine operazioni lista esercizi')

    res.send(result)
})

// registrazione
app.post('/registra', async (req, res) => {
    let result

    var workoutIn = RegistraWorkout.prototype
    workoutIn = Object.assign(RegistraWorkout.prototype, req.body)
    console.log(workoutIn)
    logger.info('requestBody: ' + workoutIn)
    try {
        result = await registraWorkoutEsercizi(workoutIn)
    } catch (error) {
        logger.error('errore durante la registrazione del workout')
        res.status(503).send({ problem: 'errore registrazione del workout' })
    }

    res.send(result)
})

// Cerca workout per Utente
app.get('/listaworkout/:idutente', async (req, resp) => {
    let result
    let idUtente

    idUtente = parseInt(req.params.idutente)
    logger.info('request param idUtente' + idUtente)
    try {
        result = await searchWorkoutByUtente(idUtente)
    } catch (error) {
        logger.error(error)
        resp.status(503).send({ problem: 'errore durante la ricerca dei workout' })
    }

    resp.send(result)
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
})