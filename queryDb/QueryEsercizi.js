const { logger } = require('../Logger')
const { Client } = require('pg')
const { configurationDb } = require('../ConfigurationDb')
const { RegistraWorkout } = require('../pojo/RegistraWorkout')
const QUERY_LISTA = 'SELECT * from esercizi'

const getListaEsercizi = async () => {
    let client = null
    let result = [];

    try {
        client = await getConnection()
        const resp = await client.query(QUERY_LISTA)
        logger.info(resp.rows)
        result = resp.rows
        logger.info(result)
    }
    catch (err) {
        console.error(err)
        logger.error(err)

        throw Error(err)
    } finally {
        client.end()
    }

    return result
}

const registraWorkoutEsercizi = async (registraWorkoutIn) => {
    let registra = RegistraWorkout.prototype
    registra = registraWorkoutIn
    const esercizi = [registra.esercizio1, registra.esercizio2, registra.esercizio3, registra.esercizio4]
    let client = null
    let result = [];
    let idWork
    let rollback
    let commit

    try {
        client = await getConnection();

        //apro procedura per transazione
        await client.query('BEGIN')
        const resp = await client.query('insert into workout (idworkout,descrizione,ripetute,utente) values(nextval(\'id_workout\'), $1, $2,$3) returning idworkout',
            [registra.descrizione, registra.tipologia, registra.utente])

        result = resp.rows
        idWork = result[0]


        for (let i = 0; i < esercizi.length; i++) {

            let esercizio = await client.query('insert into workout_esercizi (idworkout,ideserzizio) values($1,$2) returning ideserzizio',
                [idWork.idworkout, esercizi[i]])
            logger.info('inserito esercizio ' + esercizio.rows[0].ideserzizio + 'nel workout ' + idWork.idworkout + ' ' + registra.descrizione)
        }

        logger.info(idWork)
        logger.info('inserito workout con id: ' + idWork.idworkout)

        commit = await client.query('COMMIT');
        logger.info(commit)
    } catch (error) {
        logger.error(error)
        rollback = await client.query('ROLLBACK')
        logger.info(rollback)
        throw new Error(error)
    }
    finally {
        client.end()
    }
    return idWork;
}

const searchWorkoutByUtente = async (idUtente) => {
    let client = null
    let result = null;
    let data;
    try {
        client = await getConnection();
        result = await client.query('select  we.idworkout , w.descrizione as desc_workout , we.ideserzizio , e.descrizione as desc_esercizio , w.ripetute  from workout_esercizi we, workout w, esercizi e where we.idworkout = w.idworkout and we.ideserzizio = e.idesercizio  and w.utente = $1'
            , [idUtente])
        data = result.rows
    } catch (error) {
        logger.error(error)
    } finally {
        client.end()
    }
    return data
}

const getConnection = async () => {
    let client = new Client(configurationDb())
    await client.connect()
    return client
}

module.exports = { getListaEsercizi, registraWorkoutEsercizi, searchWorkoutByUtente }