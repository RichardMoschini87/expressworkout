const fs = require('fs')

const configurationDb = () => {
    const pkg = JSON.parse(fs.readFileSync('./dbconfig.json'))

    return {
        host: pkg.host,
        database: pkg.database,
        port: pkg.port,
        user: pkg.user,
        password: pkg.password
    }
}
module.exports = { configurationDb }