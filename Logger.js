const log4j = require('log4js')

log4j.configure({
    appenders: { workout: { type: "file", filename: "workout.log" } },
    categories: { default: { appenders: ["workout"], level: "info" } },
  })

  const logger = log4j.getLogger('workout')

  module.exports={logger}